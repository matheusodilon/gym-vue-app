import Vue from 'vue';
import Router from 'vue-router';
import HelloWorld from '@/components/HelloWorld';
import ClientList from '../components/Client/List/List.vue';
import ClientCreate from '../components/Client/Create/Create.vue';

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: '',
      component: ClientList
    },
    {
      path: '/client-list',
      name: 'client-list',
      component: ClientList
    },
    {
      path: '/create-client/:id',
      name: 'create-client',
      component: ClientCreate
    },
    {
      path: '/create-client',
      name: 'create-client',
      component: ClientCreate
    }
  ]
})
