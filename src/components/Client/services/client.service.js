import axios from "axios";

class ClientService {
  /**
   * Função responsável por buscar e retornar todos os clientes cadastrados na base
   */
  async getAll() {
    return axios.get('http://localhost:9000/list-all');
  }

  /**
   * Função responsável por buscar e retornar todos os clientes cadastrados na base
   */
  async getById(clientId) {
    return axios.get(`http://localhost:9000/list/${clientId}`);
  }

  /**
   * Função responsavel por cadastrar novo cliente.
   * Deve-se enviar como parametro o objeto do cliente que será cadastrado
   * @param {*} client 
   */
  async create(client) {
    return axios.post('http://localhost:9000/create', client);
  }

  /**
   * 
   * @param {*} clientId 
   * @param {*} client 
   */
  async update(clientId, client) {
    return axios.put(`http://localhost:9000/update/${clientId}`, client);
  }

  /**
   * Função responsável por excluir cliente cadastrado na base. 
   * Deve-se enviar o Id do cliente por parametro 
   * @param {*} clientId 
   */
  async delete(clientId) {
    return axios.delete(`http://localhost:9000/delete/${clientId}`);
  }
}

export default new ClientService();
